import PrimeVue from 'primevue/config'
import Paginator from 'primevue/paginator'

Vue.use(PrimeVue);

Vue.component('Paginator', Paginator);
