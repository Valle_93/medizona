window._ = require('lodash');

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
window.Vue = require('vue').default

require('./plugins/primevue')
require('./plugins/vuenumeric')


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


//Common
Vue.component('Alert', require('./components/common/Alert.vue').default)
Vue.component('ErrorsForm', require('./components/common/ErrorsForm.vue').default)
Vue.component('FilterBar', require('./components/common/FilterBar.vue').default)
Vue.component('Modal', require('./components/common/Modal.vue').default)

//Notes
Vue.component('TableNotes', require('./modules/notes/Table.vue').default)
Vue.component('FormNotes', require('./modules/notes/Form.vue').default)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
