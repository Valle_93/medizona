export default {
    data() {
        return {
            params: {
                page: 1,
                per_page: 10,
                query: ''
            }
        }
    },
    methods: {
        async changePage({ page,rows }){
            if (page === 0) {
                page = 1
            } else {
                page = page + 1
            }

            this.params.page = page
            this.params.per_page = rows

            await this.list()
        },
        async onFilterSet(filterText) {
            this.params.query = filterText
            await this.list()
        }
    }
}
