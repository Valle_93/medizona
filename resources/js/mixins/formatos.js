import accouting from 'accounting';

export default {
    methods: {
        formatoMoneda(valor){
            return accouting.formatMoney(valor)
        },
        formatoNumero(valor){
            return accouting.formatNumber(valor, 2)
        }
    }
}
