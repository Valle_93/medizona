@extends('layouts.app')

@section('content')
<div class="container mx-auto">
    <div class="shadow bg-white rounded-sm outline-none">
        <div class="p-3 border-b-2 border-gray-200">
            Home
        </div>
        <div class="p-5 text-center text-5xl">
            {{ auth()->user()->name }}
        </div>
    </div>
</div>
@endsection
