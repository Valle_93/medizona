<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" integrity="sha512-1sCRPdkRXhBV2PBLUdRb4tMg1w2YPf37qatUFeS7zlBy7jJI8Lf4VHwWfZZfpXtYSLy85pkm9GaYVYMfw5BC1A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<body>
    <div id="app">
        <nav class="flex justify-between bg-white border-gray-200 px-4 py-5 rounded mb-5">
            <h3 class="text-xl">
                <a
                    href="{{ route('home') }}"
                    class="text-zinc-900 font-medium"
                    aria-current="page"
                >
                    Home
                </a>
            </h3>
            <ul class="flex space-x-4 text-lg">
                <li>
                    <a
                        href="{{ route('notes.index') }}"
                        class="text-zinc-900"
                        aria-current="page"
                    >
                        Notas
                    </a>
                </li>
                <li>
                    <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                        Cerrar sesión
                    </a>

                    <form
                        id="logout-form"
                        action="{{ route('logout') }}"
                        method="POST"
                        class="hidden"
                    >
                        @csrf
                    </form>
                </li>
            </ul>
        </nav>

        <main class="container mx-auto px-3">
            @yield('content')
        </main>
    </div>
</body>
</html>
