@extends('layouts.auth')

@section('content-auth')
<div class="min-h-screen flex justify-center items-center">
    <form
        class="w-96 shadow-md bg-white rounded p-5"
        method="POST"
        action="{{ route('password.update') }}"
    >
        @csrf
        <h1 class="text-center text-3xl font-medium mb-6">Reseteo de contraseña</h1>
        <input type="hidden" name="token" value="{{ $token }}">
        <div class="group-input">
            <label>Email</label>
            <input
                id="email"
                type="text"
                name="email"
                class="input"
                required
                value="{{ $email ?? old('email') }}"
                autocomplete="email"
                placeholder="ingresar email"
            >
            @error('email')
                <span class="text-red-500">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="group-input">
            <label>Contraseña</label>
            <input
                id="password"
                type="password"
                class="input"
                name="password"
                placeholder="ingresar contraseña"
                required
                autocomplete="current-password"
            >
            @error('password')
                <span class="text-red-500">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="group-input">
            <label>Confirmar contraseña</label>
            <input
                id="password-confirm"
                type="password"
                class="input"
                name="password_confirmation"
                required
                placeholder="confirmar contraseña"
                autocomplete="new-password"
            />
        </div>
        <button type="submit" class="btn-primary w-full my-5">
            Resetear contraseña
        </button>
    </form>
</div>
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-end">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
