@extends('layouts.auth')

@section('content-auth')
<div class="min-h-screen flex justify-center items-center">
    <div class="w-1/3 ">
        @if (session('status'))
            <alert
                :cerrar="false"
                tipo="success"
                texto="{{ session('status') }}"
            />
        @endif
        <form
            class="shadow-md bg-white rounded p-5"
            method="POST"
            action="{{ route('password.email') }}"
        >
            @csrf
            <h1 class="text-center text-3xl font-medium mb-6">
                Resetear contraseña
            </h1>
            <div class="group-input">
                <label>Email</label>
                <input
                    id="email"
                    type="email"
                    class="input"
                    name="email"
                    value="{{ old('email') }}"
                    required
                    autocomplete="email"
                    autofocus
                />
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <button type="submit" class="btn-primary w-full my-5">
                Enviar link
            </button>
        </form>
    </div>
</div>
@endsection
