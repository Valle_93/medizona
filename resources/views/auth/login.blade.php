@extends('layouts.auth')

@section('content-auth')
<div class="min-h-screen flex justify-center items-center">
    <form
        class="w-96 shadow-md bg-white rounded p-5"
        method="POST"
        action="{{ route('login') }}"
    >
        @csrf
        <h1 class="text-center text-3xl font-medium mb-6">Login</h1>
        <div class="group-input">
            <label>Email</label>
            <input
                id="name"
                type="text"
                name="email"
                class="input"
                required
                value="{{ old('email') }}"
                placeholder="ingresar email"
            >
            @error('email')
                <span class="text-red-500">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="group-input">
            <label>Contraseña</label>
            <input
                id="password"
                type="password"
                class="input"
                name="password"
                placeholder="ingresar contraseña"
                required
                autocomplete="current-password"
            >
            @error('password')
                <span class="invalid-feedback">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="flex justify-center mt-5">
            @if (Route::has('password.request'))
                <a href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
            @endif
        </div>
        <button type="submit" class="btn-primary w-full my-5">
            Ingresar
        </button>
    </form>
</div>
@endsection
