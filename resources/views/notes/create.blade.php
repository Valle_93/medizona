@extends('layouts.app')

@section('content')
<div class="container mx-auto">
    <div class="shadow bg-white rounded-sm outline-none">
        <div class="p-3 border-b-2 border-gray-200 flex justify-between items-center">
            <h1 class="text-2xl">Nueva nota</h1>
            <a href="{{ route('notes.index') }}" class="link-button btn-gray text-gray-900">
                Regresar
            </a>
        </div>
        <div class="p-5">
            <form-notes />
        </div>
    </div>
</div>
@endsection
