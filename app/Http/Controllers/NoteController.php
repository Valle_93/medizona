<?php

namespace App\Http\Controllers;

use App\Models\Note;
use Illuminate\Http\Request;
use App\Http\Requests\NoteRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

class NoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('notes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('notes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NoteRequest $request)
    {
        DB::beginTransaction();

        try {
            $data         = $request->validated();
            $data['date'] = now();

            $note = Note::create($data);

            foreach ($request->items as $item) {
                $note->items()->attach($item['id'], [
                    'quantity' => $item['quantity'],
                    'total'    => $item['total']
                ]);
            }

            DB::commit();

            $this->message = 'Nota creada correctamente';
            $this->error   = null;
            $this->apiCode = 201;
        } catch (\Throwable $th) {
            DB::rollBack();

            $this->message = 'Nota creada correctamente';
            $this->error   = $th->getMessage();
            $this->apiCode = 500;
        }

        return response()->json([
            'error'   => $this->error,
            'message' => $this->message
        ], $this->apiCode);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $note = null;

        try {
            $columns = ['id','customer_id','date','total'];

            $note = Note::with([
                'customer:id,name',
                'items'
            ])->findOrFail($id,$columns);

            $this->error   = null;
            $this->apiCode = 200;
        } catch (\Throwable $th) {
            $this->error   = $th->getMessage();
            $this->apiCode = 404;
        }

        return response()->json([
            'error' => $this->error,
            'note'  => $note
        ], $this->apiCode);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('notes.edit', compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NoteRequest $request, $id)
    {
        DB::beginTransaction();

        try {
            $data = $request->validated();

            $note = Note::findOrFail($id);
            $note->update($data);
            $note->items()->detach();

            foreach ($request->items as $item) {
                $note->items()->attach($item['id'], [
                    'quantity' => $item['quantity'],
                    'total'    => $item['total']
                ]);
            }

            DB::commit();

            $this->message = 'Nota editada correctamente';
            $this->error   = null;
            $this->apiCode = 201;
        } catch (\Throwable $th) {
            DB::rollBack();

            $this->message = 'No se pudo editar la nota correctamente';
            $this->error   = $th->getMessage();
            $this->apiCode = 500;
        }

        return response()->json([
            'error'   => $this->error,
            'message' => $this->message
        ], $this->apiCode);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function paginate(Request $request)
    {
        $columns = ['id','customer_id','date','total'];
        $query   = $request->input('query');
        $perPage = $request->input('per_page');

        $notes = Note::when($query, function ($note, $query) {
            $note->whereHas('customer', function (Builder $customer) use ($query) {
                $customer->where('name', 'like', $query.'%');
            });
        })
        ->with('customer:id,name')
        ->orderBy('created_at')
        ->paginate($perPage,$columns);

        return response()->json(['notes' => $notes]);
    }
}
