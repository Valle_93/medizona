<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function paginate(Request $request)
    {
        $columns = ['id','name','email'];
        $query   = $request->input('query');
        $perPage = $request->input('per_page');

        $customers = Customer::when($query, function ($customer, $query) {
            $customer->where('name', 'like', $query.'%');
        })
        ->orderBy('name')
        ->paginate($perPage,$columns);

        return response()->json(['customers' => $customers]);
    }
}
