<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function paginate(Request $request)
    {
        $columns = ['id','name','sku','price'];
        $query   = $request->input('query');
        $perPage = $request->input('per_page');

        $items = Item::when($query, function ($item, $query) {
            $item->where('name', 'like', $query.'%')
                ->orWhere('sku', 'like', $query.'%');
        })
        ->orderBy('name')
        ->paginate($perPage,$columns);

        return response()->json(['items' => $items]);
    }
}
