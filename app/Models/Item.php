<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $table = 'items';

    protected $fillable = [
        'name',
        'sku',
        'price'
    ];

    //Relations
    public function notes()
    {
        return $this->belongsToMany(Note::class, 'note_items');
    }
}
